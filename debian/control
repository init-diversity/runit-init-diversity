Source: runit
Section: init-diversity
Maintainer: ProwlerGr <Prowler73@gmail.com>
Priority: optional
Standards-Version: 4.6.0
Rules-Requires-Root: no
Homepage: http://smarden.org/runit/
Build-Depends: bash-completion,
               debhelper-compat (=12),
               dh-exec,
               dh-runit (>= 2.10.2),
               dh-sysuser (>= 1.3.3~),
               dh-buildinfo (>= 0.11+nmu1),
               doc-base,
Vcs-Browser: https://salsa.debian.org/debian/runit
Vcs-Git: https://salsa.debian.org/debian/runit.git

Package: runit-init-diversity
Architecture: any
Depends: ${misc:Depends}, ${shlibs:Depends}, init-diversity-tools,
Provides: runit, runit-antix,
Replaces: runit, runit-init, runit-antix, runit-init-antix,
Breaks: runit, runit-init, runit-antix, runit-init-antix,
Conflicts: runit-init, runit-init-antix,
Recommends: grub-init-diversity, runit-service-manager,
Suggests: socklog,
Description: system-wide service supervision
 runit is a collection of tools to provide system-wide service supervision
 and to manage services.  Contrary to sysv init, it not only cares about
 starting and stopping services, but also supervises the service daemons
 while they are running.  Amongst other things, it provides a reliable
 interface to send signals to service daemons without the need for pid-files,
 and a log facility with automatic log file rotation and disk space limits.
 .
 runit service supervision can run under sysv init, systemd or replace the init
 system completely. Complete init replacement is provided by 'runit-init'
 package. Users that want to take advantage of runit supervision under systemd
 or sysv init can directly install the 'runit-run' package.


Package: getty-run
Architecture: all
Suggests: fgetty
Breaks: ${runit:Breaks}
Depends: ${misc:Depends}, ${shlibs:Depends}
Description: runscripts to supervise getty processes
 runit is a collection of tools to provide system-wide service supervision
 and to manage services.  Contrary to sysv init, it not only cares about
 starting and stopping services, but also supervises the service daemons
 while they are running.  Amongst other things, it provides a reliable
 interface to send signals to service daemons without the need for pid-files,
 and a log facility with automatic log file rotation and disk space limits.
 .
 This package provides scripts to supervise getty processes, allowing
 local login.
