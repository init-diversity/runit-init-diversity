# runit-init-diversity
-----------------------

Based on runit-antix, a heavily modified version of the upstream packaged Debian runit init & service manager.
This fork has been modified to remove package conflicts with sysvinit, allowing for both inits to co-exist on the same system.


### Recommended build instructions
---------------------------------
```
gbp clone https://gitlab.com/init-diversity/runit/runit-init-diversity.git && 
cd runit-init-diversity && 
gbp buildpackage -uc -uc
```
is the recommended method to build this package directly from git.


`sudo apt install git-buildpackage bash-completion debhelper dh-exec dh-runit dh-sysuser dh-buildinfo doc-base lowdown`

should get you all the software required to build using this method.



